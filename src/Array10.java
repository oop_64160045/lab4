
import java.util.Scanner;
import javax.swing.plaf.synth.SynthSpinnerUI;

public class Array10 {
    public static void main(String[] args) {
        int size = 0;
        int arr[];
        int uniqeSize = 0;
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input number of element: ");
        size = sc.nextInt();
        arr = new int[size];
        for (int i = 0; i < arr.length; i++) {
            System.out.print("Element " + i + ": ");
            int temp = sc.nextInt();
            int index = -1;
            for (int j = 0; j < uniqeSize; j++) {
                if (arr[j] == temp) {
                    index = j;
                }
            }
            if (index < 0) {
                arr[uniqeSize] = temp;
                uniqeSize++;
            }
        }
        System.out.print("All number: ");
        for (int i = 0; i < uniqeSize; i++) {
            System.out.print(arr[i] + " ");

        }
    }
}
